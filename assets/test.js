QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});

QUnit.test('Testing calculate function with several sets of inputs', function (assert) {
    assert.strictEqual(cal(100,10),1000,"Calculation");
    assert.strictEqual(cal(10,-50),"undefined","Calculation");
    assert.strictEqual(cal(-1,10),"undefined","Calculation");
    assert.strictEqual(cal(0,10),0,"Calculation");
});
QUnit.test('Testing calculate function with several sets of inputs', function (assert) {
    assert.strictEqual(calv(100),75,"Calculation");
    assert.strictEqual(calv(50),37.5,"Calculation");
    assert.strictEqual(calv(10),7.5,"Calculation");
    assert.strictEqual(calv(0),0,"Calculation");
});
